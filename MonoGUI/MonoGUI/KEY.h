// KEY.h: define all KEY_ key value;
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
// 本文件定义了全部键值
#if !defined(__KEY_H__)
#define __KEY_H__

#if defined (RUN_ENVIRONMENT_WIN32)
// 定义Win32环境下的键值

// 数字键
#define KEY_0				0X30 
#define KEY_1				0X31
#define KEY_2				0X32
#define KEY_3				0X33
#define KEY_4				0X34
#define KEY_5				0X35
#define KEY_6				0X36
#define KEY_7				0X37
#define KEY_8				0X38
#define KEY_9				0X39
#define KEY_00				0X71

// 字母键
#define KEY_A               0X41
#define KEY_B               0X42
#define KEY_C               0X43
#define KEY_D               0X44
#define KEY_E               0X45
#define KEY_F               0X46
#define KEY_G               0X47
#define KEY_H               0X48
#define KEY_I               0X49
#define KEY_J               0X4A
#define KEY_K               0X4B
#define KEY_L               0X4C
#define KEY_M               0X4D
#define KEY_N               0X4E
#define KEY_O               0X4F
#define KEY_P               0X50
#define KEY_Q               0X51
#define KEY_R               0X52
#define KEY_S               0X53
#define KEY_T               0X54
#define KEY_U               0X55
#define KEY_V               0X56
#define KEY_W               0X57
#define KEY_X               0X58
#define KEY_Y               0X59
#define KEY_Z               0X5A

// 功能键
#define KEY_F1				0X70
#define KEY_F2				0X71
#define KEY_F3              0X72
#define KEY_F4              0X73
#define KEY_F5              0X74
#define KEY_F6              0X75
#define KEY_F7              0X76
#define KEY_F8              0X77
#define KEY_F9              0X78
#define KEY_F10             0X79
#define KEY_F11             0X7A
#define KEY_F12             0X7B

// 方向键
#define KEY_LEFT            0X25
#define KEY_UP              0x26
#define KEY_RIGHT           0X27
#define KEY_DOWN            0X28

// 符号键
#define KEY_SUB             0XBD	// (-_)
#define KEY_EQUALS          0XBB	// (=+)
#define KEY_SEMICOLON       0XBA	// (;:)
#define KEY_QUOTATION       0XDE    // ('")
#define KEY_BACK_QUOTE      0XC0	// (`~)
#define KEY_BACK_SLASH      0XDC	// (\|)
#define KEY_DIVIDE          0XBF	// (/?)
#define KEY_COMMA           0XBC	// (,<)
#define KEY_PERIOD          0XBE	// (.>)
#define KEY_OPEN_BRACKET    0XDB	// ([{)
#define KEY_CLOSE_BRACKET   0XDD	// (]})

// 编辑控制键
#define KEY_SPACE           0X20
#define KEY_BACK_SPACE      0X08
#define KEY_TAB             0x09
#define KEY_INSERT          0X2D
#define KEY_DELETE          0X2E
#define KEY_HOME            0X24
#define KEY_END             0X23
#define KEY_PAGE_UP         0X21
#define KEY_PAGE_DOWN       0X22

// 其他按键
#define KEY_ENTER           0X0D
#define KEY_ESCAPE          0X1B
#define KEY_CONTROL         0X11
#define KEY_SHIFT           0X10
#define KEY_PAUSE           0X13	// (BREAK)
#define KEY_CAPS_LOCK       0X14
#define KEY_NUM_LOCK        0X90

#endif	// Win32环境下的键值定义结束

#if defined(RUN_ENVIRONMENT_LINUX)
// 定义linux环境下的键值（请根据具体的嵌入式系统环境进行修改）

// 数字键
#define KEY_00              0X17
#define KEY_0               0X24 
#define KEY_1               0X25
#define KEY_2               0X18
#define KEY_3               0X57
#define KEY_4               0X26
#define KEY_5               0X19
#define KEY_6               0X10
#define KEY_7               0X2c
#define KEY_8               0X1e
#define KEY_9               0X11

// 功能键
#define KEY_F11             0X6c	//输入法
#define KEY_F12             0x44	//切换输入法

// 方向键
#define KEY_LEFT            0X09
#define KEY_UP              0X05
#define KEY_RIGHT           0X03
#define KEY_DOWN            0X06

// 符号键
#define KEY_PERIOD          0X56	// (.)

// 编辑控制键
#define KEY_SPACE           0X21	// 空格
#define KEY_BACK_SPACE      0X37	// 退格
#define KEY_TAB             0X16	// 切换
#define KEY_DELETE          0X0c	// 删除
#define KEY_PAGE_UP         0x3c    //PAGE_UP
#define KEY_PAGE_DOWN       0x6b    //PAGE_DOWN
#define KEY_ENTER           0X15	// 回车
#define KEY_ESCAPE          0X13	// ESC

//OCombo用到,暂定义为空
#define KEY_HOME            0x00
#define KEY_END             0xff

#endif	// linux环境下的键值定义结束

// 特殊按键掩码
// mask key flag
#define SHIFT_MASK          0x01
#define CAPSLOCK_MASK       0x10

#endif // !defined(__KEY_H__)

